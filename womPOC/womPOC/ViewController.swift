//
//  ViewController.swift
//  womPOC
//
//  Created by Miguel Garcia on 2016-09-05.
//  Copyright © 2016 Miguel Garcia. All rights reserved.
//

import UIKit
import DropDown

class ViewController: UIViewController {
    
    @IBOutlet weak var selectionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnBolsas: UIButton!
    
    let dropDown = DropDown()
    
    let pickerData = ["Datos","Voz", "SMS", "Mixtas", "Tematicas", "Roaming"]
    let bolsas = [1, 3, 4, 10, 0, 20]
    
    var optionsView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize = self.view.frame.size
        selectionLabel.text = ""
        btnBolsas.setTitle("Seleccione ...", forState: UIControlState.Normal)
        setupDropDown()
        
        // Inicio con opcion seleccionada
        self.selectedOption(0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupDropDown() {
        dropDown.anchorView = btnBolsas
        dropDown.dataSource = pickerData
        dropDown.bottomOffset = CGPoint(x: 0, y: btnBolsas.bounds.height)
        
        dropDown.selectionAction = { [unowned self] (index, item) in
            self.selectedOption(index)
        }
    }
    
    func selectedOption(row: Int) {
        selectionLabel.text = "Has Seleccionado la Bolsa: \(pickerData[row])"
        btnBolsas.setTitle("\(pickerData[row])", forState: UIControlState.Normal)
        
        optionsView.removeFromSuperview()
        optionsView = showOptions(row, columnas: 2)
        
        let frm = CGRectMake(0, 0, self.view.frame.width, optionsView.frame.height + 200)
        scrollView.contentSize = frm.size
        scrollView.addSubview(optionsView)
    }
    
    func showOptions(id: Int, columnas: Int) -> UIView {
    
        // ****
        // Debe reemplazado por la llamada al serivcio:
        // 1. Se debe obtener el count del array.
        // 2. Obtener los valores de cada opcion (MB, Vigencia, Precio, etc.)
        // ****
        let total_opts = bolsas[id]
    
        optionsView = UIView()
        var total_h = CGFloat(0)
        
        if (total_opts != 0) {
        
            let v_width = self.view.frame.width
            let space_x = CGFloat(18)
            let space_y = CGFloat(18)
            
            let op_w = ( v_width - (space_x * CGFloat(columnas + 1)) ) / CGFloat(columnas)
            let op_h = op_w * 1.2 // Alto de cada opcion. 1.2 veces el ancho.
                
            var opt_row = CGFloat(1)
            var posy = CGFloat(0)
            
            for _ in 0...(total_opts - 1) {
                
                let posx = (space_x * opt_row) + (op_w * (opt_row - 1))
                
                // ****
                // Reemplazar por el UIView por cada Opcion.
                // Construir el UIView por opcion (con los valores asociados).
                // ****
                let opt = UIView(frame: CGRectMake(posx, posy, op_w, op_h))
                opt.backgroundColor = UIColor.grayColor()
                
                
                // Animacion
                opt.frame.origin.y += 20
                opt.alpha = CGFloat(0)
                UIView.animateWithDuration(0.5, delay: 0, options: [], animations: {
                    opt.frame.origin.y = posy
                    opt.alpha = CGFloat(100)
                }, completion: nil)
                
                optionsView.addSubview(opt)
                
                if (Int(opt_row) == columnas) {
                    posy = (posy + space_y) + op_h
                    total_h = posy
                    opt_row = 1
                }
                else {
                    total_h = posy + op_h
                    opt_row++
                }
            }
        }
        
        let view_posy = CGFloat(130)
        optionsView.frame = CGRectMake(0, view_posy, self.view.frame.width, total_h + 18)

        return optionsView
    }

    @IBAction func selectOption(sender: AnyObject) {
        dropDown.show()
    }
}
